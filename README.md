# BIM & Scan® Third-Party Library (CGAL)

![BIM & Scan](BimAndScan.png "BIM & Scan® Ltd.")

Conan build script for [CGAL](https://www.cgal.org/), the 'Computational Geometry Algorithms Library'. Built with TBB support for parallel operations.

Supports version 4.13 (stable).

Requires the [Bincrafters](https://bintray.com/bincrafters/public-conan) and [BIM & Scan® (public)](http://bsdev-jfrogartifactory.northeurope.cloudapp.azure.com/artifactory/webapp/) Conan repositories for third-party dependencies.
