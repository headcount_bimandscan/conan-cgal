@ECHO OFF

REM
REM 2018-2019 © BIM & Scan® Ltd.
REM See 'README.md' in the project root for more information.
REM

CALL conan upload -c -r "bimandscan-public" --all "cgal/4.13@bimandscan/stable"

@ECHO ON
