#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile


class CGAL(ConanFile):
    name = "cgal"
    version = "4.13"
    license = "LGPL-3.0"
    url = "https://bitbucket.org/headcount_bimandscan/conan-cgal"
    description = "The 'Computational Geometry Algorithms Library'."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "https://www.cgal.org/"

    _name_upper = "CGAL"
    _src_dir = f"{name}-releases-{_name_upper}-{version}"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "shared": [
                                True,
                                False
                            ],
                  "fPIC": [
                              True,
                              False
                          ],
                  "paging_safe": [
                                     True,
                                     False
                                 ]
              }

    default_options = "shared=False", \
                      "fPIC=True", \
                      "paging_safe=False"

    exports = "../LICENCE.md"

    requires = "gmp/6.1.2@bincrafters/stable", \
               "tbb/2019_U2@bimandscan/stable", \
               "boost_system/1.69.0@bincrafters/stable", \
               "boost_atomic/1.69.0@bincrafters/stable", \
               "boost_chrono/1.69.0@bincrafters/stable", \
               "boost_date_time/1.69.0@bincrafters/stable", \
               "boost_thread/1.69.0@bincrafters/stable", \
               "boost_any/1.69.0@bincrafters/stable", \
               "eigen/3.3.7@conan/stable", \
               "mpfr/4.0.1@bimandscan/stable", \
               "boost_property_map/1.69.0@bincrafters/stable"

    build_requires = "cmake_findboost_modular/1.69.0@bincrafters/stable"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        zip_name = f"{self._src_dir}.zip"
        src_cmakefile = f"{self._src_dir}/CMakeLists.txt"

        tools.download(f"https://github.com/CGAL/cgal/archive/releases/{self._name_upper}-{self.version}.zip",
                       zip_name)

        tools.unzip(zip_name)
        os.unlink(zip_name)

        # Patch CMake configuration:
        tools.replace_in_file(src_cmakefile,
                              "project(CGAL CXX C)",
                              "project(CGAL CXX C)\ninclude(\"${CMAKE_BINARY_DIR}/conanbuildinfo.cmake\")\nconan_basic_setup()\n")

        # Patch code for specific custom data type compatibility.
        if self.options.paging_safe:
            jet_file = f"{self._src_dir}/Point_set_processing_3/include/CGAL/jet_estimate_normals.h"
            pca_file = f"{self._src_dir}/Point_set_processing_3/include/CGAL/pca_estimate_normals.h"

            tools.replace_in_file(jet_file,
                                  "namespace CGAL {",
                                  "#undef CGAL_LINKED_WITH_TBB\nnamespace CGAL {")

            tools.replace_in_file(jet_file,
                                  "#endif",
                                  "#define CGAL_LINKED_WITH_TBB\n#endif")

            tools.replace_in_file(pca_file,
                                  "namespace CGAL {",
                                  "#undef CGAL_LINKED_WITH_TBB\nnamespace CGAL {")

            tools.replace_in_file(pca_file,
                                  "#endif",
                                  "#define CGAL_LINKED_WITH_TBB\n#endif")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions["BUILD_DOC"] = False
        cmake.definitions["BUILD_TESTING"] = False

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC

        # Enable/disable CGAL modules:
        cmake.definitions["WITH_CGAL_Core"] = True
        cmake.definitions["WITH_CGAL_ImageIO"] = False
        cmake.definitions["WITH_CGAL_Qt5"] = False
        cmake.definitions["WITH_GMP"] = True
        cmake.definitions["WITH_GMPXX"] = False
        cmake.definitions["WITH_MPFR"] = True
        cmake.definitions["WITH_Eigen3"] = True

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure(source_folder = self._src_dir)
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        install_docdir = f"{self._src_dir}/Installation"

        self.copy("LICENSE",
                  "licenses",
                  install_docdir)

        self.copy("LICENSE.GPL",
                  "licenses",
                  install_docdir)

        self.copy("LICENSE.LGPL",
                  "licenses",
                  install_docdir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.compiler == "Visual Studio":
            self.cpp_info.cppflags.extend([
                                              "/openmp",
                                              "/DCGAL_LINKED_WITH_TBB"
                                          ])

            self.cpp_info.cflags.extend([
                                            "/openmp",
                                            "/DCGAL_LINKED_WITH_TBB"
                                        ])
        else:
            self.cpp_info.cppflags.extend([
                                              "-fopenmp",
                                              "-DCGAL_LINKED_WITH_TBB"
                                          ])

            self.cpp_info.cflags.extend([
                                            "-fopenmp",
                                            "-DCGAL_LINKED_WITH_TBB"
                                        ])

        if self.settings.os != "Windows":
            self.cpp_info.libs.append("pthread")
            self.cpp_info.cppflags.append("-pthread")
            self.cpp_info.cflags.append("-pthread")
        elif self.settings.compiler == "Visual Studio":
            self.cpp_info.cppflags.append("/DNOMINMAX")
            self.cpp_info.cflags.append("/DNOMINMAX")
        else:
            self.cpp_info.cppflags.append("-DNOMINMAX")
            self.cpp_info.cflags.append("-DNOMINMAX")

        if self.settings.os == "Linux" and \
           self.settings.compiler == "gcc":

            self.cpp_info.libs.append("m")
