#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 © BIM & Scan® Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans.model.conan_file import ConanFile

from conans import CMake


class PkgTest_CGAL(ConanFile):
    name = "pkgtest_cgal"
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    requires = "cgal/4.13@bimandscan/stable"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("*.so*",
                  dst="bin",
                  src="lib")

        self.copy("*.dll",
                  dst="bin",
                  src="bin")

        self.copy("*.dylib",
                  dst="bin",
                  src="lib")

    def test(self):
        os.chdir("bin")
        #self.run(f".{os.sep}{self.name}")
        # TODO(neil): fix broken TBBMalloc test run in pkgtest
