/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <CGAL/CORE_Expr.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Delaunay_triangulation_2.h>

using Real = CORE::Expr;
using Kernel = CGAL::Simple_cartesian<Real>;
using Delaunay = CGAL::Delaunay_triangulation_2<Kernel>;
using Point2D = Kernel::Point_2;


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'CGAL' package test (compilation, linking, and execution).\n";

    Delaunay dt;

    const Point2D p(0.0,
                    0.0),
                  q(std::sqrt(2.0),
                    1.0),
                  r(0.0,
                    1.0);

    dt.insert(p);
    dt.insert(q);
    dt.insert(r);
    std::cout << "\nDelaunay triangulation test (from example code)...\n\n" << dt << std::endl;

    std::cout << "'CGAL' package works!" << std::endl;
    return EXIT_SUCCESS;
}
